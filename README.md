# microk8-for-tf

[vRA Cloud Template](https://blogs.vmware.com/management/2020/08/vmware-cloud-templates.html) to deploy single node K8 (Containerd) for Terraform Runtime Interface.

The [vRealize Automation on-premises](https://blogs.vmware.com/management/2021/02/announcing-vmware-vrealize-automation-8-3.html) product requires users to configure their own **Terraform runtime Interface.**
The runtime environment consists of a Kubernetes cluster that runs Terraform CLI commands to perform requested operations. In addition, the runtime collects logs and returns the results from Terraform CLI commands.

However some organizations may not have a Kubernetes cluster available for many reasons, specially during lab or evaluation stages, as an alternative you could leverage the same vRealize Automation with the Cloud Template provided in this repository for deploying a single node Kubernetes master/worker enviroment (no Docker required) to fulfill this requirement and even use for any other Kubernetes testing relatesd, e.g. own Kubernetes education and/or test vRealize Automation Kubernetes zone, vRealize Operations Management Pack for Kubernetes, etc.

You can find more information about [preparing a vRealize Automation Cloud Assembly Terraform runtime environment](https://docs.vmware.com/en/vRealize-Automation/8.3/Using-and-Managing-Cloud-Assembly/GUID-56EFBD96-C294-4ED7-B162-E1059CE3DF0E.html)

And for using the [Terraform Service in vRA ( Cloud Templates )](https://blogs.vmware.com/management/2020/09/terraform-service-in-vra.html)

IMPORTANT, This effort is targeted for lab testing and it is not Production ready, for that [Tanzu Kubernetes Solutions](https://tanzu.vmware.com/tanzu?utm_source=google&utm_medium=cpc&utm_campaign=amer_c1_b&utm_content=g2_t023&utm_term=vmware%20tanzu&_bt=473987194529&_bk=vmware%20tanzu&_bm=e&_bn=g&_bg=112035911176&gclid=Cj0KCQiAvP6ABhCjARIsAH37rbSMPFU8-yhCRW2tmYzravUy0Z6yB1P3yu-sDSEXz-vEqgAFFvxNY1MaAh28EALw_wcB) are recommended.

**REQUIREMENTS:**

This Cloud Template has been tested with the following releases:

- Image: [Ubuntu 18.04.5 LTS](urlhttps://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.ova) and/or Ubuntu 18.04.1 LTS with Cloud-Init enabled.
- vRA 8.2 and/or 8.3 GA (You need to have it configured and ready for deploying Cloud Templates).
- Minimum Flavor Recommendations: CPU: 2 Cores | RAM 4 GB

**OPERATION DETAILS:**

- The Cloud Template allows you to create a new user and provide its credentials.
- A default Kubernetes Namespace **vravmware** is created for the Terraform Runtime Integration.
- The Cloud Template allows you to set a shared "Tag Constraint" for Compute and Network, you could edit further to meet your specific enviroment.
- When deployed, the Cloud Template will generate a customized **kubeconfig** named **vmware.config** available at user home, that you could cat, copy and paste at the Terraform Runtime Integration's External kubeconfig input. 
- Ther are two Cloud Template Variants:
    1. Agnostic (Using an exiting Ubuntu 18.04 Image and Flavor)
    2. vSphere Based, which allows you to load the referenced Ubuntu Image from the OVA.
- The Kubernetes release is **1.16** and it is enabled by microk8.

**TROUBLESHOOTING:**


- Verify that the flavor, image selected and "Tag Constraints" meet the ones available at your [vRealize Automation on-premises](https://blogs.vmware.com/management/2021/02/announcing-vmware-vrealize-automation-8-3.html) enviroment, e.g. Default Agnostic Cloud Template Image: **Ubuntu-18** may be different at your setup.
- Verify that Cloud-Init completes succesfully, inspect log `/var/log/cloud-init-output.log` for details, as errors could depend on how your image was staged, as an alternative you could use the vSphere version with the default OVA image.
- Verify that the Kubernetes node is working with command `kubectl get nodes -o wide` status should show **Ready**
- Also issue command `kubectl cluster-info` it should indicate that **Kubernetes master** is running and listening.
- Verify the namespace is created with command `kubectl get namespaces` the default namespace **vravmware** or the one you input is listed. 
- Verify that Dynamic DNS is stopped and disabled with command:  `systemctl status systemd-resolved` and your `/etc/resolved` file shows your DNS.

**RELEASE:**

Version: 1.0
Updated: Feb 7 2020





